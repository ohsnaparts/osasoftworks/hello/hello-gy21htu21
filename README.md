# GY21 HTU21

The sensor is an  easy to use highly accurate digital humidity andtemperature sensor.This sensor provides calibrated, linearized signals indigital format.It has a variety of applications in indoor and outdoor weather stations,environment/data   centers,   automotive   climate   control   and   defoggingdevices, home appliances, medical equipment, printers, humidifiers, mobilephones, tablets and many more. The resolution of the digital humidity sensor can be changed by command.Temperature digital output resolution from  12  up to  14  bit and humiditydigital output resolution from 8 up to 12 bit.One of the features of the module is an energy saving operation, which hasa very low current consumption for the result.

## Docs

For additional information about the sensor used in this project, please see the [docs](docs) folder