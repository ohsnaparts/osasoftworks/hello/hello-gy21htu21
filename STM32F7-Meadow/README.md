# Introduction

Periodically reads temperature and humidity from the I2C bus and prints it to serial console.

# Screenshot

![](./images/vs-output.gif)