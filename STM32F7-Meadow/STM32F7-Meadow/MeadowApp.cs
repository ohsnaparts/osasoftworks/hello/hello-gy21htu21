﻿using Meadow;
using Meadow.Devices;
using Meadow.Foundation.Leds;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Meadow.Foundation.Maple.Web.Client;
using Meadow.Foundation.Sensors.Atmospheric;
using Meadow.Foundation.Sensors.Light;
using Meadow.Foundation.Web.Maple.Server;
using Meadow.Hardware;
using Microsoft.MinIoC;
using STM32F7_Meadow.Exceptions;
using STM32F7_Meadow.Initializers;
using STM32F7_Meadow.IoC;
using STM32F7_Meadow.Networking;
using Exception = System.Exception;


namespace STM32F7_Meadow
{
    public sealed class MeadowApp : App<F7Micro, MeadowApp>, IDisposable
    {
        private readonly ICollection<IDisposable> _subscriptions = new HashSet<IDisposable>(1);
        private readonly IEnvironmentalService _environmentalService;
        private readonly Temt6000 _ambientLightSensor;
        private readonly Htu21d _atmosphereSensor;
        private MapleServer _server;


        public MeadowApp() : this(SetupContainer())
        {
        }

        private MeadowApp(Container container)
        {
            try
            {
                Console.WriteLine("Initializing hardware...");
                _ambientLightSensor = container.Resolve<Temt6000>();
                _atmosphereSensor = container.Resolve<Htu21d>();
                _environmentalService = container.Resolve<IEnvironmentalService>();

                // TODO: Secure Credentials
                SetupNetworkAsync(new WifiCredentials("Ssid", "Passphrase")).Wait();

                EnsureWifiAdapter();
                
                SetupMapleServer(Device.WiFiAdapter!.IpAddress);
                PrintGreetingAsync().Wait();

                Run();
            }
            catch (Exception ex)
            {
                Dispose();
                Console.Error.WriteException(ex);
                throw;
            }
        }

        private void EnsureWifiAdapter()
        {
            if (Device.WiFiAdapter == default)
                throw new ConnectionException("Unable to continue with uninitialized wifi adapter.");
        }

        /// <summary>Demonstration on how to use MapleClient</summary>
        private async Task PrintGreetingAsync()
        {
            if (Device.WiFiAdapter == default)
            {
                Console.WriteLine("Wifi adapter uninitialized, unable to fetch greeting. Silently continuing...");
                return;
            }

            var greeting = await new MapleClient().GetAsync(
                hostAddress: Device.WiFiAdapter.IpAddress.ToString(),
                port: 5417,
                endpoint: "hello",
                parameters: new Dictionary<string, string> {{"name", "master"}}
            );

            Console.WriteLine("--------------------");
            Console.WriteLine(greeting);
            Console.WriteLine("--------------------");
            Console.WriteLine();
        }

        private static Container SetupContainer()
        {
            Console.WriteLine("Initializing dependency container...");
            var container = RootContainerProvider.Container;

            container.Register<F7Micro>(() => Device).AsSingleton();
            container.Register<ILedService>(() => new LedService(
                new RgbPwmLed(
                    container.Resolve<F7Micro>(),
                    container.Resolve<F7Micro>().Pins.OnboardLedRed,
                    container.Resolve<F7Micro>().Pins.OnboardLedGreen,
                    container.Resolve<F7Micro>().Pins.OnboardLedBlue,
                    3.3f, 3.3f, 3.3f,
                    Meadow.Peripherals.Leds.IRgbLed.CommonType.CommonAnode
                ))
            );

            container.Register(() =>
            {
                var device = container.Resolve<F7Micro>();
                var inputPort = device.CreateAnalogInputPort(Device.Pins.A05);
                return new Temt6000(inputPort);
            }).AsSingleton();

            container.Register<II2cBus>(() =>
            {
                var device = container.Resolve<F7Micro>();
                return device.CreateI2cBus();
            }).AsSingleton();

            container.Register(() => new Htu21d(container.Resolve<II2cBus>(), 0x40)).AsSingleton();
            container.Register<IEnvironmentalService>(() => new EnvironmentalService(
                container.Resolve<Htu21d>(),
                container.Resolve<Temt6000>()
            )).AsSingleton();


            return container;
        }

        private void SetupMapleServer(IPAddress ip, int port = 5417)
        {
            _server = new MapleServer(
                ip,
                port,
                processMode: RequestProcessMode.Parallel
            );

            _server.Start();
        }

        private async Task SetupNetworkAsync(WifiCredentials credentials)
        {
            try
            {
                InitializeNetworkConnection.InitializeAdapter(Device);
                await InitializeNetworkConnection.ConnectToNetworkAsync(
                    Device.WiFiAdapter,
                    credentials.Ssid,
                    credentials.Passphrase
                );
            }
            catch (Exception ex)
            {
                throw new ConnectionException("An error occured trying to initialize a wifi connection.", ex);
            }
        }

        private void Run()
        {
            _ambientLightSensor.StartUpdating(TimeSpan.FromSeconds(1));
            _atmosphereSensor.StartUpdating(TimeSpan.FromSeconds(1));

            Console.WriteLine("Running...");
            var subscription = _environmentalService.EnvironmentalReadings.Subscribe(
                reading =>
                {
                    Console.WriteLine(reading);
                    Thread.Sleep(1000);
                });

            _subscriptions.Add(subscription);
        }

        public void Dispose()
        {
            foreach (var subscription in _subscriptions)
                subscription?.Dispose();

            _ambientLightSensor.StopUpdating();
            _atmosphereSensor.Dispose();
            _server.Stop();
        }
    }
}