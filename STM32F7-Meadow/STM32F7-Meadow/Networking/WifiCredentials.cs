﻿namespace STM32F7_Meadow.Networking
{
    public class WifiCredentials
    {
        public WifiCredentials(string ssid, string passphrase)
        {
            Ssid = ssid;
            Passphrase = passphrase;
        }

        public string Ssid { get; }
        public string Passphrase { get; }
    }
}