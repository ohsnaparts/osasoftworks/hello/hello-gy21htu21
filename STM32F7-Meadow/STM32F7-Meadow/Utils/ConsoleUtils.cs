﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace STM32F7_Meadow
{
    public static class ConsoleUtils
    {
        public static void WriteException(this TextWriter writer, Exception exception)
        {
            IEnumerable<string> ExceptionMessages(Exception ex, uint level = 0)
            {
                if (ex != null)
                    yield return $"Message Level {level}: {ex.Message}\n";
                else
                {
                    yield break;
                }

                var innerMessages = ExceptionMessages(ex.InnerException, ++level);
                foreach (var message in innerMessages)
                    yield return message;
            }

            var messages = ExceptionMessages(exception)
                .Prepend("---------- Exception begin ----------")
                .Append("---------- Exception end ----------");

            writer.WriteLine(string.Join('\n', messages));
        }
    }
}