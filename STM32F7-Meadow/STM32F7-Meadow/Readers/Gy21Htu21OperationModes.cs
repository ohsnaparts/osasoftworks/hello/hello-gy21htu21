﻿namespace STM32F7_Meadow
{
    public enum Gy21Htu21OperationModes
    {
        TriggerTemperatureMeasurementHoldMaster = 0xE3,
        TriggerHumidityMeasurementHoldMaster = 0xE5,
        TriggerTemperatureMeasurementNoHoldMaster = 0xF3,
        TriggerHumidityMeasurementNoHoldMaster = 0xF5,
        WriteUserRegister = 0xE6,
        ReadUserRegister = 0xE7,
        SoftReset = 0xFE
    }
}