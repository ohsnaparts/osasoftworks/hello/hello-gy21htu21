﻿using Meadow.Hardware;
using Meadow.Units;

namespace STM32F7_Meadow.Math.Translators
{
    public class Temt6000DataReader
    {
        private readonly II2cBus _bus;
        private readonly byte _sensorAddress;

        public Temt6000DataReader(II2cBus bus, byte sensorAddress)
        {
            _bus = bus;
            _sensorAddress = sensorAddress;
        }

        private void SetOperationMode(Gy21Htu21OperationModes mode) => _bus.WriteData(_sensorAddress, (byte)mode);
        private byte[] ReadRawData() => _bus.ReadData(_sensorAddress, 2);

        public RelativeHumidity ReadHumidity()
        {
            //Algorithm translated from chip producers datasheet
            RelativeHumidity ToHumidity(byte[] data)
            {
                int high = data[0];
                int low = data[1];

                int container = high / 100;
                high = high % 100;

                double rawData1 = container * 25600;
                double rawData2 = high * 256 + low;

                rawData1 = (125 * rawData1) / 65536;
                rawData2 = (125 * rawData2) / 65536;

                var value = rawData1 + rawData2 - 6;
                return new RelativeHumidity(value);
            }

            SetOperationMode(Gy21Htu21OperationModes.TriggerHumidityMeasurementHoldMaster);
            return ToHumidity(ReadRawData());
        }

        private Temperature ReadTemperature(II2cBus bus, byte address)
        {
            //Algorithm translated from chip producers datasheet
            Temperature ToTemperature(byte[] data)
            {
                var high = data[0] << 8;
                var low = data[1];
                var signalOutputTemp = high + low;
                var celsius = -46.85 + (175.72 * signalOutputTemp) / System.Math.Pow(2, 16);

                return new Temperature(celsius);
            }

            SetOperationMode(Gy21Htu21OperationModes.TriggerTemperatureMeasurementHoldMaster);
            return ToTemperature(ReadRawData());
        }
    }
}