﻿using Meadow.Units;

namespace STM32F7_Meadow.ReadingTypes
{
    public struct AtmosphericReading
    {
        public Temperature? Temperature { get; set; }
        public RelativeHumidity? Humidity { get; set; }
    }
}