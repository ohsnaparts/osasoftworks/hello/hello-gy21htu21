﻿using System;
using Meadow.Units;

namespace STM32F7_Meadow
{
    public struct EnvironmentalReading
    {
        public RelativeHumidity? Humidity { get; set; }
        public Temperature? Temperature { get; set; }
        public Illuminance AmbientLight { get; set; }

        public override string ToString() => string.Format(
            "Temperature: {0}C, Humidity: {1}RH% Light: {2}Lux",
            Temperature?.Celsius, 
            Humidity?.Percent, 
            AmbientLight.Lux
        );
    }
}