﻿using System;
using System.Net;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using Cairo;
using Meadow.Foundation.Web.Maple.Server;
using Meadow.Foundation.Web.Maple.Server.Routing;
using Microsoft.MinIoC;
using STM32F7_Meadow.IoC;

namespace STM32F7_Meadow.Controllers
{
    public class MapleController : RequestHandlerBase
    {
        private Container Container => RootContainerProvider.Container;

        [HttpGet]
        public void Hello()
        {
            string name = QueryString["name"] ?? "Anonymous";

            var _ledService = Container.Resolve<ILedService>();
            _ledService.ShowRandomColor();

            Context.Response.ContentType = ContentTypes.Application_Text;
            Context.Response.StatusCode = (int) HttpStatusCode.OK;
            Send($"Hey there {name}♥").Wait();
        }


        [HttpGet]
        public void Current()
        {
            var environmentalService = Container.Resolve<IEnvironmentalService>();
            
            var reading = environmentalService.EnvironmentalReadings
                .Take(1)
                .ToTask()
                .Result;

            Console.WriteLine($"Retrieved reading: {reading}");

            Context.Response.ContentType = ContentTypes.Application_Text;
            Context.Response.StatusCode = (int) HttpStatusCode.OK;
            Send(reading).Wait();
        }
    }
}