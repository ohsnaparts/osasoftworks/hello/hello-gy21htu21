﻿using System;
using System.Reactive.Linq;
using Meadow;
using Meadow.Foundation.Sensors.Atmospheric;
using Meadow.Foundation.Sensors.Light;
using Meadow.Units;

namespace STM32F7_Meadow
{
    public interface IEnvironmentalService
    {
        IObservable<EnvironmentalReading> EnvironmentalReadings { get; }
    }

    internal sealed class EnvironmentalService : IEnvironmentalService
    {
        public EnvironmentalService(Htu21d atmosphereSensor, Temt6000 ambientLightSensor)
        {
            TemperatureReadings = BuildTemperatureObservable(atmosphereSensor);
            HumidityReadings = BuildHumidityObservable(atmosphereSensor);
            AmbientLight = BuildBrightnessObservable(ambientLightSensor);
            EnvironmentalReadings =
                BuildEnvironmentalReadingObservable(TemperatureReadings, HumidityReadings, AmbientLight);
        }

        public IObservable<EnvironmentalReading> EnvironmentalReadings { get; }
        public IObservable<IChangeResult<Temperature>> TemperatureReadings { get; }
        public IObservable<IChangeResult<RelativeHumidity>> HumidityReadings { get; }
        public IObservable<ChangeResult<Illuminance>> AmbientLight { get; }


        private IObservable<ChangeResult<Illuminance>> BuildBrightnessObservable(Temt6000 ambientLightSensor) =>
            Observable.FromEvent<EventHandler<IChangeResult<Voltage>>, IChangeResult<Voltage>>(
                handler => (sender, result) => handler(result),
                addHandler => ambientLightSensor.Updated += addHandler,
                removeHandler => ambientLightSensor.Updated -= removeHandler
            ).Select(update => new ChangeResult<Illuminance>(
                update.New.ToTemt6000Illuminance(),
                update.Old?.ToTemt6000Illuminance()
            ));

        private IObservable<EnvironmentalReading> BuildEnvironmentalReadingObservable(
            IObservable<IChangeResult<Temperature>> temperature,
            IObservable<IChangeResult<RelativeHumidity>> humidity,
            IObservable<ChangeResult<Illuminance>> ambientLight
        ) => temperature.Zip(
            humidity,
            ambientLight,
            (temperatureReading, humidityReading, ambientLightReading) => new EnvironmentalReading
            {
                Temperature = temperatureReading.New,
                Humidity = humidityReading.New,
                AmbientLight = ambientLightReading.New
            });


        private IObservable<IChangeResult<RelativeHumidity>> BuildHumidityObservable(Htu21d sensor) =>
            Observable.FromEvent<EventHandler<IChangeResult<RelativeHumidity>>, IChangeResult<RelativeHumidity>>(
                handler => (sender, result) => handler(result),
                addHandler => sensor.HumidityUpdated += addHandler,
                removeHandler => sensor.HumidityUpdated -= removeHandler
            );

        private IObservable<IChangeResult<Temperature>> BuildTemperatureObservable(Htu21d sensor) =>
            Observable.FromEvent<EventHandler<IChangeResult<Temperature>>, IChangeResult<Temperature>>(
                handler => (sender, result) => handler(result),
                addHandler => sensor.TemperatureUpdated += addHandler,
                removeHandler => sensor.TemperatureUpdated -= removeHandler
            );
    }
}