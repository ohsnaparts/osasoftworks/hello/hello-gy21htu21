﻿using System;
using Meadow.Foundation;
using Meadow.Foundation.Leds;

namespace STM32F7_Meadow
{
    public interface ILedService
    {
        void ShowRandomColor();
    }

    public class LedService : ILedService
    {
        private readonly RgbPwmLed _led;

        public LedService(RgbPwmLed led)
        {
            _led = led;
        }

        public void ShowRandomColor() => ShowColor(new Random().Next(0xFFFFFF));
        public void ShowColor(int hex) => ShowColor(hex.ToString("X"));
        public void ShowColor(string hex) => ShowColor(Color.FromHex(hex));

        public void ShowColor(Color color)
        {
            _led.Stop();
            _led.SetColor(color);
        }
    }
}