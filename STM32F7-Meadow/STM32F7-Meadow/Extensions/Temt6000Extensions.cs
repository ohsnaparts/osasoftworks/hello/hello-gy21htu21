﻿using Meadow.Units;
using STM32F7_Meadow.Math.Units;

namespace STM32F7_Meadow
{
    public static class Temt6000Extensions
    {
        private static readonly Resistance Resistance = Resistance.FromOhms(10000);

        public static Illuminance ToTemt6000Illuminance(this Voltage voltage)
        {
            // V = I * R
            var current = new Current(voltage.Volts / Resistance.Ohms);
            var lux = new Illuminance(current.Microamps * 2);

            return lux;
        }
    }
}