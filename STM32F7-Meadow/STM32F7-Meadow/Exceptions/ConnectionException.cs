﻿using System;

namespace STM32F7_Meadow.Exceptions
{
    public class ConnectionException : ApplicationException
    {
        public ConnectionException(string message) : this(message, null)
        {
        }

        public ConnectionException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}