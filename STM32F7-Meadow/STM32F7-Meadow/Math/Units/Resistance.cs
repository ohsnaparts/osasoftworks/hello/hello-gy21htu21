﻿using Meadow.Units;

namespace STM32F7_Meadow.Math.Units
{
    public class Resistance : IUnitType
    {
        public int Ohms { get; set; }

        public static Resistance FromOhms(int ohms) => new Resistance(ohms);
        private Resistance(int ohms)
        {
            Ohms = ohms;
        }
    }
}