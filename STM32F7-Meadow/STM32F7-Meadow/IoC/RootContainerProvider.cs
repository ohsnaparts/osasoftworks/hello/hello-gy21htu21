﻿using System;
using Microsoft.MinIoC;

namespace STM32F7_Meadow.IoC
{
    internal sealed class RootContainerProvider
    {
        private static Lazy<Container> LazyContainer { get; } = new Lazy<Container>(
            InitializeContainer
        );

        public static Container Container => LazyContainer.Value;
        
        private static Container InitializeContainer()
        {
            var container = new Container();

            container.Register(() => new MeadowApp());

            return new Container();
        }
    }
}