﻿using System;
using System.Threading.Tasks;
using Meadow.Devices;
using Meadow.Gateway.WiFi;
using Meadow.Gateways;
using STM32F7_Meadow.Exceptions;

namespace STM32F7_Meadow.Initializers
{
    public class InitializeNetworkConnection
    {
        public static void InitializeAdapter(F7MicroBase device)
        {
            Console.WriteLine("Initializing wifi adapter");
            var isWifiInitSuccessful = device.InitWiFiAdapter().Result;
            if (!isWifiInitSuccessful)
                throw new Exception("An attempt to initialize the Wifi adapter failed silently.");
        }

        public static async Task ConnectToNetworkAsync(IWiFiAdapter wifiAdapter, string ssid, string password)
        {
            Console.WriteLine("Initializing wifi adapter");
            if (wifiAdapter == null)
                throw new ArgumentNullException($"{nameof(wifiAdapter)}",
                    "Unable to connect to network because Wifi Adapter is null.");

            var connection = await wifiAdapter.Connect(ssid, password);
            if (connection.ConnectionStatus != ConnectionStatus.Success)
                throw new ConnectionException(
                    $"An attempt to connect to wifi network {ssid} failed with ConnectionStatus {ConnectionStatus.Success}."
                );

            Console.WriteLine($"Connected to IP: {wifiAdapter.IpAddress}");
        }
    }
}